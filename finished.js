$(document).ready(function () {

    //correct answer local storage
    let storageIndex1 = localStorage.getItem('index1') == null ? '0' : localStorage.getItem('index1');
    let storageIndex2 = localStorage.getItem('index2') == null ? '0' : localStorage.getItem('index2');
    let storageIndex3 = localStorage.getItem('index3') == null ? '0' : localStorage.getItem('index3');
    let storageIndex4 = localStorage.getItem('index4') == null ? '0' : localStorage.getItem('index4');
    let storageIndex5 = localStorage.getItem('index5') == null ? '0' : localStorage.getItem('index5');
    let storageIndex6 = localStorage.getItem('index6') == null ? '0' : localStorage.getItem('index6');
    let storageIndex7 = localStorage.getItem('index7') == null ? '0' : localStorage.getItem('index7');
    let storageIndex8 = localStorage.getItem('index8') == null ? '0' : localStorage.getItem('index8');
    let storageIndex9 = localStorage.getItem('index9') == null ? '0' : localStorage.getItem('index9');
    let storageIndex10 = localStorage.getItem('index10') == null ? '0' : localStorage.getItem('index10');

    let correct_answer = parseInt(storageIndex1) + parseInt(storageIndex2) + parseInt(storageIndex3) + parseInt(storageIndex4) + parseInt(storageIndex5) + parseInt(storageIndex6) + parseInt(storageIndex7) + parseInt(storageIndex8) + parseInt(storageIndex9) + parseInt(storageIndex10)

    //skip answer local storage

    let storageSkip1 = localStorage.getItem('skip1') == null ? '0' : localStorage.getItem('skip1');
    let storageSkip2 = localStorage.getItem('skip2') == null ? '0' : localStorage.getItem('skip2');
    let storageSkip3 = localStorage.getItem('skip3') == null ? '0' : localStorage.getItem('skip3');
    let storageSkip4 = localStorage.getItem('skip4') == null ? '0' : localStorage.getItem('skip4');
    let storageSkip5 = localStorage.getItem('skip5') == null ? '0' : localStorage.getItem('skip5');
    let storageSkip6 = localStorage.getItem('skip6') == null ? '0' : localStorage.getItem('skip6');
    let storageSkip7 = localStorage.getItem('skip7') == null ? '0' : localStorage.getItem('skip7');
    let storageSkip8 = localStorage.getItem('skip8') == null ? '0' : localStorage.getItem('skip8');
    let storageSkip9 = localStorage.getItem('skip9') == null ? '0' : localStorage.getItem('skip9');
    let storageSkip10 = localStorage.getItem('skip10') == null ? '0' : localStorage.getItem('skip10');

    let skip_answer = parseInt(storageSkip1) + parseInt(storageSkip2) + parseInt(storageSkip3) + parseInt(storageSkip4) + parseInt(storageSkip5) + parseInt(storageSkip6) + parseInt(storageSkip7) + parseInt(storageSkip8) + parseInt(storageSkip9) + parseInt(storageSkip10)

    //wrong answer local storage

    let storageError1 = localStorage.getItem('error1') == null ? '0' : localStorage.getItem('error1');
    let storageError2 = localStorage.getItem('error2') == null ? '0' : localStorage.getItem('error2');
    let storageError3 = localStorage.getItem('error3') == null ? '0' : localStorage.getItem('error3');
    let storageError4 = localStorage.getItem('error4') == null ? '0' : localStorage.getItem('error4');
    let storageError5 = localStorage.getItem('error5') == null ? '0' : localStorage.getItem('error5');
    let storageError6 = localStorage.getItem('error6') == null ? '0' : localStorage.getItem('error6');
    let storageError7 = localStorage.getItem('error7') == null ? '0' : localStorage.getItem('error7');
    let storageError8 = localStorage.getItem('error8') == null ? '0' : localStorage.getItem('error8');
    let storageError9 = localStorage.getItem('error9') == null ? '0' : localStorage.getItem('error9');
    let storageError10 = localStorage.getItem('error10') == null ? '0' : localStorage.getItem('error10');

    let wrong_answer = parseInt(storageError1) + parseInt(storageError2) + parseInt(storageError3) + parseInt(storageError4) + parseInt(storageError5) + parseInt(storageError6) + parseInt(storageError7) + parseInt(storageError8) + parseInt(storageError9) + parseInt(storageError10)

    let name = localStorage.getItem('name')

    $(".correct").append(correct_answer);
    $(".skip-answer").append(skip_answer);
    $(".wrong-answer").append(wrong_answer);
    $(".name").append(name);

})
